Avec Fanny, Florent et Jean-Emmanuel nous avons discut� au sujet de
s�ances de lecture et discussion d'articles en lien � la fois avec les
th�matiques de GLACE et de CYME.

Nous sommes partis sur un format avec une discussion hebdomadaire sur un
papier s�inscrivant dans un cycle th�matique de 3 � 5 semaines. L'id�e est
donc d'avoir une continuit� dans les papiers et de mettre en place un
syst�me p�renne.

Par la suite, quand le rythme sera �tabli, cela pourra bien sur �voluer
vers un format diff�rent avec des papiers plus d'actualit�s et des
suggestions de chacun.

Les discussions sont ouvertes � toutes et tous, le public vis� est plut�t
les non-permanents avec bien sur la participation de permanents concern�s
en fonction de th�matiques. Bien que les th�matiques ne soient pas
toujours dans nos cordes ou nos sujets c'est l'occasion de discuter et
d�couvrir ce qui se fait autour de nous. Il serait donc bien d'avoir la
plupart des th�sards lors de ces discussions.

Le premier cycle a aussi pour objectif d'inciter les M2 � se joindre �
nous. Nous avons donc choisi comme th�me:
 "La d�formation de la glace et l'�coulement des glaciers "
avec les papiers suivants:

----
1- Glenn, 1958 (article cl� sur la d�formation de la glace - m�thode de
laboratoire + physique analytique)
2- Harper, 2001 (caract�risation de la d�formation � l'�chelle du glacier
- m�thode de terrain)
3- Armstrong, 2016 (variations temporelle de la vitesse des glaciers et
lien avec le glissement basal - imagerie satellitaire et mod�lisation
simple)
4- (A voir en fonction de la dynamique)
----

Pour l'instant nous sommes partis sur le cr�neau du: *Mercredi � 15h* avec
des discussions d'une petite heure. La premi�re devrait avoir lieu le *7
f�vrier*.

N'h�sitez pas � nous renvoyer vos commentaires et suggestions.

